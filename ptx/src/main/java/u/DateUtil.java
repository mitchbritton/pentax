package u;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateUtil {

	private static String plural(long v) {
		return v > 1 ? "s " : "";
	}

	public static LocalDateTime toLocalDateTime(String dateStr) {
		try {
			return toLocalDateTime(dateStr, "dd-MM-yyyy");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return LocalDateTime.now();
	}

	public static LocalDateTime toLocalDateTime(String dateStr, String format) throws ParseException {
		return dateToLocalDateTime(dateFromString(dateStr, format));
	}

	public static Date dateFromString(String dateStr, String format) throws ParseException {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
		return dateFormatter.parse(dateStr);
	}

	public static LocalDateTime dateToLocalDateTime(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	public static Date localDateTimeToDate(LocalDateTime time) {
		if (time == null) {
			return new Date(0); // a long time ago
		}
		Instant instant = time.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	public static String getElapsedTimeFromSeconds(long elapsed) {
		String s = "";
		long hours = elapsed / 3600;
		if (hours > 0) {
			s += hours + " hour" + plural(hours) + " ";
		}
		long remain = elapsed % 3600;
		long mins = remain / 60;
		if (mins > 0) {
			s += mins + " min" + plural(mins) + " ";
		}
		remain %= 60;
		s += remain + " secs";
		return s;
	}
}