package mab.pentax.cmd;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.regex.PatternSyntaxException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import mab.pentax.base.CameraException;
import mab.pentax.base.SyncTask;
import u.DateUtil;

@Slf4j
@Getter
@Setter
public class Sync extends SyncTask {
	private final static String DATE_ARG = "-d=";
	private final static String DIR_ARG = "-o=";
	private final static String MATCH_ARG = "-m=";

	public static void main(String[] args) throws CameraException {
		log.info("Sync run");
		Sync s = new Sync();
		s.processArgs(args);
		s.run();
	}

	public Sync() {
		super();
	}

	public static void usage(String msg) {
		System.out.println("Usage: java " + Sync.class.getName() + DIR_ARG + "<directory> [" + DATE_ARG
				+ "<date as DD-MM-YYYY> ]");
		if (msg != null)
			System.out.println(msg);
		System.exit(-1);
	}

	public void processArgs(String[] args) {
		String dateStr = null;
		String dir = null;
		String match = null;

		for (String arg : args) {
			if (arg.startsWith(DATE_ARG))
				dateStr = arg.substring(DATE_ARG.length());
			else if (arg.startsWith(DIR_ARG))
				dir = arg.substring(DIR_ARG.length());
			else if (arg.startsWith(MATCH_ARG))
				match = arg.substring(MATCH_ARG.length());
			else
				usage("Unknown arg");
		}
		if (dateStr != null) {
			if ("now".equals(dateStr))
				setDate(LocalDateTime.now());
			else
				setDate(DateUtil.toLocalDateTime(dateStr));
		}
		if (match != null) {
			try {
				setMatch(match);
			} catch (PatternSyntaxException e) {
				usage("Pattern " + match + " is invalid");
			}
		}
		if (dir == null) {
			usage("No directory");
		}
		Path dirp = Paths.get(dir);
		if (!Files.isDirectory(dirp)) {
			usage(dir + " is not a directory");
		}
		setDir(dir);
	}

	@Override
	public void output(String msg) {
		System.out.println(msg);
	}
}
