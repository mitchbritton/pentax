package mab.pentax.base;

public class CameraException extends Exception {
	private static final long serialVersionUID = 1L;
	public static final int NO_CAMERAS_FOUND = 1;

	private int code;

	public CameraException(int code) {
		this.code = code;
	}

	public String getMessage() {
		switch (code) {
		case NO_CAMERAS_FOUND:
			return "No cameras found";
		}
		return "Unknown camera exception";
	}
}
