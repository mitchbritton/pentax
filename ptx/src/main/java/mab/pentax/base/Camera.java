package mab.pentax.base;

import static mab.pentax.base.CameraException.NO_CAMERAS_FOUND;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.ricoh.camera.sdk.wireless.api.CameraDevice;
import com.ricoh.camera.sdk.wireless.api.CameraDeviceDetector;
import com.ricoh.camera.sdk.wireless.api.CameraImage;
import com.ricoh.camera.sdk.wireless.api.DeviceInterface;
import com.ricoh.camera.sdk.wireless.api.response.Response;
import com.ricoh.camera.sdk.wireless.api.response.Result;

import lombok.extern.slf4j.Slf4j;
import u.DateUtil;

@Slf4j
public class Camera {
	CameraDevice device;

	public Camera() throws CameraException {
		device = getCamera();
		if (device == null) {
			throw new CameraException(NO_CAMERAS_FOUND);
		}
	}

	public boolean connect() {
		Response resp = device.connect(DeviceInterface.WLAN);
		Result result = resp.getResult();
		return "OK".equals(result.name());
	}

	private static CameraDevice getCamera() {
		log.info("Getting cameras");
		List<CameraDevice> cameras = CameraDeviceDetector.detect(DeviceInterface.WLAN);
		if (cameras.isEmpty()) {
			return null;
		}
		return cameras.get(0);
	}

	public List<CameraImage> getImages() {
		return getImages(null);
	}

	public void transferImageToFile(CameraImage image, String fileName) {
		try (FileOutputStream outputStream = new FileOutputStream(new File(fileName))) {
			Response response = image.getData(outputStream);
			log.info(response.getResult().name());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
	}

	public List<CameraImage> getImages(final LocalDateTime time) {
		List<CameraImage> v = device.getImages();
		List<CameraImage> out = new ArrayList<>();
		log.info("There are {} images\n - date is {}", v.size(), time);
		Iterator<CameraImage> it = v.iterator();
		Date date = DateUtil.localDateTimeToDate(time);
		// use iterator to avoid concurrent mod exception - shit library
		while (it.hasNext()) {
			CameraImage image = it.next();
			Date when = image.getDateTime();
			if (when == null) {
				log.debug("Image {} has a null date!", image.getName());
			} else if (when.after(date)) {
				log.debug("{} has date of {}", image.getName(), when);
				out.add(image);
			}
		}
		return out;
	}
}
