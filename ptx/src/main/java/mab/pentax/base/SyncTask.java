package mab.pentax.base;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ricoh.camera.sdk.wireless.api.CameraImage;

import jdk.internal.org.jline.utils.Log;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
abstract public class SyncTask implements Runnable {

	private LocalDateTime date = null;
	private String dir;
	private String match;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Pattern pattern;

	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Camera cam;

	public SyncTask() {
	}

	public void setMatch(String match) {
		this.match = match;
		pattern = Pattern.compile(match);
	}

	private boolean match(String name) {
		Matcher m = pattern.matcher(name);
		return m.find();
	}

	private boolean fileExists(String name) {
		Path path = Paths.get(getFileName(name));
		return Files.exists(path);
	}

	private String getFileName(String name) {
		return dir + File.separator + name;
	}

	abstract public void output(String msg);

	public void run() {
		try {
			cam = new Camera();
			if (cam.connect()) {
				process();
			}
		} catch (CameraException e1) {
			Log.warn(e1.getMessage());
		}
	}

	private List<CameraImage> removeNonMatchesAndAlreadyProcessed(List<CameraImage> images) {
		List<CameraImage> out = new LinkedList<>();
		for (CameraImage image : images) {
			String name = image.getName();
			if (match(name) && !fileExists(name)) {
				out.add(image);
			}
		}
		return out;
	}

	private void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	private void download(List<CameraImage> images) {
		CameraImage image;
		for (int i = 0; images.size() > 0; i++) {
			int index;
			switch (i % 3) {
			case 0:
				index = 0;
				break;
			case 1:
				index = images.size() - 1;
				break;
			default:
				index = images.size() / 2;
				break;
			}
			image = images.get(index);
			images.remove(index);
			String fileName = getFileName(image.getName());
			output(String.format("Downloading %s", fileName));
			cam.transferImageToFile(image, fileName);
		}
	}

	private void process() {
		while (true) {
			List<CameraImage> images = cam.getImages(date);
			log.debug("There are {} images after {}", images.size(), date);
			images = removeNonMatchesAndAlreadyProcessed(images);
			log.debug("After removal there are {} images left", images.size());
			download(images);
			sleep(2000);
		}
	}
}
